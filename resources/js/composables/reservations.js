import {ref} from 'vue'
import axios from "axios";

export default function useReservations() {
    const reservation = ref([]);
    const reservations = ref([]);

    const errors = ref('')

    const getReservations = async () => {
        let response = await axios.get('api/reservations')
        reservations.value = response.data.data
    }

    const getReservation = async (id) => {
        let response = await axios.get(`api/reservations/${id}`)
        reservation.value = response.data.data
    }

    return {
        errors,
        reservation,
        reservations,
        getReservations,
        getReservation
    }
}
