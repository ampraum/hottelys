<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\HotelRequest;
use App\Http\Resources\HotelResource;
use App\Models\Hotel;
use App\Repository\HotelRepositoryInterface;

class HotelController extends Controller
{
    /**
     * @var HotelRepositoryInterface
     */
    private $hotelRepository;

    public function __construct(HotelRepositoryInterface $hotelRepository)
    {
        $this->hotelRepository = $hotelRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return HotelResource::collection($this->hotelRepository->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param HotelRequest $request
     * @return HotelResource
     */
    public function store(HotelRequest $request)
    {
        $hotel = $this->hotelRepository->create($request->validated());

        return new HotelResource($hotel);

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Hotel $hotel
     * @return \Illuminate\Http\Response
     */
    public function show(Hotel $hotel)
    {
        $hotel = $this->hotelRepository->findById($hotel->id, ['*'], ['rooms']);

        return new HotelResource($hotel);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Hotel $hotel
     * @return \Illuminate\Http\Response
     */
    public function update(HotelRequest $request, Hotel $hotel)
    {
        $this->hotelRepository->update($hotel->id, $request->validated());

        return new HotelResource($hotel);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Hotel $hotel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hotel $hotel)
    {
        $this->hotelRepository->deleteById($hotel->id);
        return response()->noContent();
    }
}
