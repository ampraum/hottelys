<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https:/password/img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Repo info
### Backend
- [Laravel + php8] (https://laravel.com/)
- [Laravel Sail (Development stack with docker and docker-compose)] (https://laravel.com/docs/8.x/sail)

### Frontend
- [Vue 3 + axios] (https://v3.vuejs.org/)
- [tailwindcss] (https://tailwindcss.com/)

## Requirements

- Docker and docker compose installed
- Port 80 available or modify it in the _.env_ file

## Prepare the environment:

### Copy .env file
```
- cp .env.example .env
- Modify the *APP_PORT* if necessary
```
### Install composer using a container
- docker-compose -f docker-compose.helper.yml run --rm sailcomposer composer install --ignore-platform-reqs

## Run the containers
```
bash vendor/bin/sail up -d
```

## Create tables
```
bash vendor/bin/sail artisan migrate
```

## Populate demo data
```
bash vendor/bin/sail artisan migrate --seed
```

## The APP

- Open the browser [localhost](http://localhost).
- [Create an user](http://localhost/register)


## Enjoy the app


## TO DO
There are still things to do:
- Load reservations with secure-api
- Crud for rooms and reservations
- Apply repository pattern to classes that are needed
- Test please, phpunit please!

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
