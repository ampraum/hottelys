<?php

namespace App\Repository\Eloquent;

use App\Models\Hotel;
use App\Repository\HotelRepositoryInterface;

class HotelRepository extends BaseRepository implements HotelRepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct(Hotel $model)
    {
        $this->model = $model;
    }
}
