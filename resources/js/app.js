require('./bootstrap');

require('alpinejs');

import {createApp} from "vue";
import router from './router'

import HotelsIndex from './components/hotels/HotelsIndex.vue'

createApp({
    components: {
        HotelsIndex
    }
}).use(router).mount('#app')
