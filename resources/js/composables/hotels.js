import {ref} from 'vue'
import axios from 'axios'
import {useRouter} from 'vue-router'

export default function useHotels() {
    const hotel = ref([])
    const hotels = ref([])
    const rooms = ref([]);

    const errors = ref('')
    const router = useRouter()

    const getHotels = async () => {
        let response = await axios.get('/api/hotels')
        hotels.value = response.data.data
    }

    const getHotel = async (id) => {
        let response = await axios.get(`/api/hotels/${id}`)
        hotel.value = response.data.data
        rooms.value = response.data.data.rooms;
    }

    const storeHotel = async (data) => {
        errors.value = ''
        try {
            await axios.post('/api/hotels', data)
            await router.push({name: 'hotels.index'})
        } catch (e) {
            if (422 == e.response.status) {
                for (const key in e.response.data.errors) {
                    errors.value += e.response.data.errors[key][0] + ' ';
                }
            }
        }
    }

    const updateHotel = async (id) => {
        errors.value = ''
        try {
            await axios.patch(`/api/hotels/${id}`, hotel.value)
            await router.push({name: 'hotels.index'})
        } catch (e) {
            if (e.response.status === 422) {
                for (const key in e.response.data.errors) {
                    errors.value += e.response.data.errors[key][0] + ' ';
                }
            }
        }
    }

    const destroyHotel = async (id) => {
        await axios.delete(`/api/hotels/${id}`)
    }

    return {
        errors,
        hotel,
        rooms,
        hotels,
        getHotel,
        getHotels,
        storeHotel,
        updateHotel,
        destroyHotel
    }
}
