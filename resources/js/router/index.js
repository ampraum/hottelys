import {createRouter, createWebHistory} from 'vue-router'

import HotelsIndex from '../components/hotels/HotelsIndex.vue'
import HotelCreate from '../components/hotels/HotelCreate'
import HotelEdit from '../components/hotels/HotelEdit'
import HotelShow from '../components/hotels/HotelShow'
import ReservationsIndex from '../components/reservations/ReservationsIndex'

const routes = [
    {
        path: '/dashboard',
        name: 'hotels.index',
        component: HotelsIndex
    },
    {
        path: '/hotels/create',
        name: 'hotels.create',
        component: HotelCreate
    },
    {
        path: '/hotels/:id/edit',
        name: 'hotels.edit',
        component: HotelEdit,
        props: true
    },
    {
        path: '/hotels/:id/show',
        name: 'hotels.show',
        component: HotelShow,
        props: true
    },
    {
        path: '/reservations',
        name: 'reservations.index',
        component: ReservationsIndex,
    }
];

export default createRouter({
    history: createWebHistory(),
    routes
})
