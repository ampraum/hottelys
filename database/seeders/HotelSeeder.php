<?php

namespace Database\Seeders;

use App\Models\Hotel;
use App\Models\Room;
use Illuminate\Database\Seeder;

class HotelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hotels = [
            [
                'name' => 'Ayre Hotel Gran Vía',
                'email' => 'ayre@hottelys.wip',
                'city' => 'Barcelona',
                'address' => 'Gran Vía de les Corts Catalanes 322, Sants-Montjuïc, 08004',
                'website' => 'https://hottelys.wip/hotel/ayre-hotel-gran-via'
            ],
            [
                'name' => 'Riu Plaza España',
                'email' => 'riuplaza@hottelys.wip',
                'city' => 'Madrid',
                'address' => 'Calle Gran Vía, 84, Centro de Madrid, 28013',
                'website' => 'https://hottelys.wip/hotel/riu-plaza-espana'
            ],
            [
                'name' => 'Eurostars Catedral',
                'email' => 'eurostars@hottelys.wip',
                'city' => 'Granada',
                'address' => '11 Calle Cárcel Baja, Centro de Granada, 18010',
                'website' => 'https://hottelys.wip/hotel/eurostars-catedral'
            ],
            [
                'name' => 'Silken Al-Andalus Palace',
                'email' => 'silken@hottelys.wip',
                'city' => 'Sevilla',
                'address' => 'Paraná, Bellavista - Palmera, 41012',
                'website' => 'https://hottelys.wip/hotel/eurostars-catedral'
            ],
            [
                'name' => 'Hotel Malcom and Barret',
                'email' => 'hmalcom@hottelys.wip',
                'city' => 'Valencia',
                'address' => 'Avenida Ausias March, 59, Quatre Carreres, 46013',
                'website' => 'https://hottelys.wip/hotel/hotel-malcom'
            ],
            [
                'name' => 'Ibis Budget Málaga Aeropuerto Avenida de Velazquez',
                'email' => 'ibisbudget@hottelys.wip',
                'city' => 'Málaga',
                'address' => 'Horacio Quiroga, 33, Carretera de Cádiz, 29004',
                'website' => 'https://hottelys.wip/hotel/ibis-budget'
            ],
            [
                'name' => 'Hotel Cordoba Center',
                'email' => 'hcordoba@hottelys.wip',
                'city' => 'Córdoba',
                'address' => 'Avenida de la Libertad, 4 Esq. Avenida Gran Capitan, 14006',
                'website' => 'https://hottelys.wip/hotel/h-cordoba-center'
            ]
        ];

        foreach ($hotels as $hotel) {
            Hotel::create($hotel);
        }
    }
}
